const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const User = require('../models/users.model.js');
const moment = require('moment');
router.get('/',(req,res) =>{
  res.redirect('/')
});

//get All users from db and send it to client
router.get('/getUsers',(req,res) =>{
  User.find({}, function(err, users) {
    if (err) throw err;

    // object of all the users
    res.send(users);
  });
});

//Save new User
router.post('/newUser',(req,res)=>{

  console.log("User Data",req.body);

  // create a new user called newUser
  let newUser = new User({
    name:{
      first:req.body.firstname,
      last:req.body.lastname
    },
    birthday:moment(req.body.birthday).subtract(1,'month').toISOString(),
    gender:req.body.gender,
    customerLifetimeValue:req.body.cltvalue
  });

  //call save method to save data
  newUser.save(function(err) {
    if (err) throw err;
    console.log('User saved successfully!');
    res.send({});
  });
});


//find user by id for edit operation
router.post('/getUser',(req,res) =>{
  User.findById({_id:req.body.id},(err,user) =>{
    if(err) throw err;

    res.send(user);
  });


//update user
router.put('/update',(req,res) =>{
  User.findById({_id:req.body.id},(err,user) =>{
    if(err) throw err;

    user.name.first = req.body.data.firstname;
    user.name.last = req.body.data.lastname;
    user.birthday = moment(req.body.data.birthday).subtract(1,'month').toISOString();
    user.gender = req.body.data.gender;
    user.customerLifetimeValue = req.body.data.cltvalue;

    // save the user
    user.save(function(err) {
      if (err){
        throw err;
      }
      res.send({});
    });

  })
});
});

router.post('/remove',(req,res) =>{
  console.log('Remove Called',req.body.id);
  User.findById({_id:req.body.id},(err,user) => {
    if (err) throw err;
    user.remove((err) =>{
      if(err) throw err;

      console.log('Removed successfully');
      res.send({});
    })
  })

});
module.exports = router;

// grab the things we need
const mongoose = require('mongoose');
mongoose.connect('mongodb://sachin:sachin123@ds121461.mlab.com:21461/restoke');
const Schema = mongoose.Schema;

// create a schema
const userSchema = new Schema({
  name: {
    first:String,
    last:String
  },
  birthday:Date,
  gender:String,
  lastContact: String ,
  customerLifetimeValue:Number
});

userSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.lastContact = currentDate;
  next();
});

// the schema is useless so far
// we need to create a model using it
const User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;

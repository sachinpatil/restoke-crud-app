import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Params} from '@angular/router';

@Injectable()
export class DataService {

  constructor(private __httpService:HttpClient) { }

  getAllUsers(){
    return this.__httpService.get('/getUsers');
  }

  saveUserInfo(userInfo){
    return this.__httpService.post('/newUser',userInfo.value);
  }

  getSingleUser(id){
    return this.__httpService.post('/getUser',id);
  }

  updateUserInfo(value){
    return  this.__httpService.put('/update',value);
  }

  removeUser(id){
    console.log("Delete",id);
    return this.__httpService.post('/remove',id);
  }
}

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../services/data.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import * as moment from 'moment';
@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  genders:string[] = ['male','female'];
  birthday = {
    year:null,
    month:null,
    day:null
  };
  editMode:boolean = false;
  signUpForm:FormGroup;
  param:Params = {};
  mindate = {
    year:1960,
    month:1,
    day:1
  };
  constructor(private __dataService:DataService,private activatedRoute:ActivatedRoute,private route:Router) { }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      'firstname':new FormControl(null,Validators.required),
      'lastname':new FormControl(null,Validators.required),
      'birthday':new FormControl(null,Validators.required),
      "gender":new FormControl('male'),
      'cltvalue':new FormControl(null,Validators.required)
    });

    this.activatedRoute.params.subscribe(
      (param) =>{
        this.param = param;
        if(Object.keys(param).length !== 0){
          this.editMode = true;
          this.__dataService.getSingleUser(param).subscribe(
            (data) =>{
              this.birthday = {
                year:moment(data['birthday']).year(),
                month:moment(data['birthday']).add(1,'month').month(),
                day:moment(data['birthday']).date()
              };
              this.signUpForm.patchValue({
                firstname:data['name'].first,
                lastname:data['name'].last,
                birthday:this.birthday,
                gender:data['gender'],
                cltvalue:data['customerLifetimeValue']
              })
            }
          )
        }
      }
    )


  }

  onSubmit(){
    //save user data
    this.__dataService.saveUserInfo(this.signUpForm).subscribe(
      (data) =>{
        console.log("User Saved successfully")
      }
    );

    //reset form after submission
    this.signUpForm.reset({
      'firstname':'',
      "lastname":'',
      "birthday":"",
      "gender":"male",
      "ctlvalue":""
    })
  }

  onUpdate(){
    this.__dataService.updateUserInfo({id:this.param.id,data:this.signUpForm.value}).subscribe(
        (data) =>{
          console.log(data);
          this.route.navigate(['/details']);
        },
      (error) =>{console.log("Error is",error);}
      )
  }
}

import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import * as _ from "underscore";
import * as moment from 'moment';
import {Router} from '@angular/router';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  allUsers:any = [];
  constructor(private dataService:DataService,private router:Router) { }

  ngOnInit() {
    this.getData();
  }

  onEdit(id){
    this.router.navigate(['/edit',id])
  }

  removeUser(id){
    this.dataService.removeUser({id:id}).subscribe(
      (data) =>{
        console.log('Data',data);
        this.getData();
      }
    )
  }

  getData(){
    this.dataService.getAllUsers().subscribe(
      (data) =>{
        _.map(data,(user) =>{
          user.birthday = moment(user.birthday).format('DD-MM-YYYY');
        });
        this.allUsers = data;
      }
    )
  }
}

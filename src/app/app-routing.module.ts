import {RouterModule, Routes} from '@angular/router';
import {NewUserComponent} from './new-user/new-user.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {NgModule} from '@angular/core';
import {HomeComponent} from './home/home.component';

const APP_ROUTES: Routes = [
  {path:'',component:HomeComponent},
  {path: 'new', component: NewUserComponent},
  {path: 'edit/:id', component: NewUserComponent},
  {path: 'details', component: UserDetailsComponent},
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule{

}
